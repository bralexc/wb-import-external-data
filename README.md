# WB Import External Load #

A couple of scripts to export transient data from a rotating model in ANSYS CFD-Post  and import them back to ANSYS Mechanical.

## Usage ##

There are two steps for this: exporting the CFD data and importing it back into Mechanical.

### Export CFD transient data ###

First export the data for the CFD simulation.

* Open CFD transient rotor simulation in CFD-Post.
* Open `export_data_cfdpost.cse` in a text editor.
* Set `$start_timestep`, `$end_timestep` and `$skip_timestep` to the iteration number you want to start and end exporting data, as well as the number of timesteps to skip between them.
* Set `$boundary_name` to the name of the boundary condition where you want to export the data. This can be a list of names separated by comma.
* Set `$filepath` to the folder where files should be stored. Mind the forward slashes.
* Save your changes.
* Load `export_data_cfdpost.cse` in CFD-Post through **Session > Play Session** (if running CFD-Post in standalone mode) of copy/paste the entire script into the window from **Tools > Command Editor**

### Import data into Mechanical ###
Now its time to import files back into ANSYS Workbench.

* First, open `import_external_data.wbjn` in a text editor.
* Set `filepath`, `start_timestep`, `end_timestep` and `skip_timestep`, as before.
* Set `start_data_line`, `file_variable`, `angular_velocity`, `cfd_timestep` according to the comments in the file.
* Set `basename` to the file base name. `XX` will be replaced by sequetial number given by `start_timestep`, `end_timestep` and `skip_timestep`.
* Set `mech_system_name` to the name of the Mechanical System to be used. This can be grabbed by right-clicking the system, selecting **Properties** and looking at either of the fields **System ID** or **Directoty Name**.
* Run `import_external_data.wbjn` in ANSYS Workbench through **File > Scripting > Run Script File**.

## Requirements ##

* ANSYS Workbench 17.2 and above.